# 
# Usage: To re-create this platform project launch xsct with below options.
# xsct /home/uho/workspace/Vitis-AI/dsa/DPU-TRD/DPU_2core_B4096/platform.tcl
# 
# OR launch xsct and run below command.
# source /home/uho/workspace/Vitis-AI/dsa/DPU-TRD/DPU_2core_B4096/platform.tcl
# 
# To create the platform in a different location, modify the -out option of "platform create" command.
# -out option specifies the output directory of the platform project.

platform create -name {DPU_2core_B4096}\
-hw {/home/uho/workspace/Vitis-AI/dsa/DPU-TRD/prj/Vivado/prj/top_wrapper.xsa}\
-proc {psu_cortexa53_0} -os {standalone} -arch {64-bit} -fsbl-target {psu_cortexa53_0} -out {/home/uho/workspace/Vitis-AI/dsa/DPU-TRD}

platform write
platform generate -domains 
platform active {DPU_2core_B4096}
platform active {DPU_2core_B4096}
platform generate
platform active {DPU_2core_B4096}
platform generate
platform generate
platform active {DPU_2core_B4096}
platform generate
domain active {zynqmp_fsbl}
bsp reload
domain active {standalone_domain}
bsp reload
domain active {zynqmp_pmufw}
bsp reload
bsp reload
