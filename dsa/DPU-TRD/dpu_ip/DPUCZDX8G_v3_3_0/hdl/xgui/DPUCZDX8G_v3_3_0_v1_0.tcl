# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  set Page_0 [ipgui::add_page $IPINST -name "Page 0"]
  ipgui::add_param $IPINST -name "ARCH_DATA_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ARCH_HP_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ARCH_ICP" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ARCH_IMG_BKGRP" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ARCH_OCP" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ARCH_PP" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CLK_GATING_ENA" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CONV_DSP_ACCU_ENA" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CONV_DSP_CASC_MAX" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CONV_LEAKYRELU" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CONV_RELU6" -parent ${Page_0}
  ipgui::add_param $IPINST -name "CONV_WR_PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DNNDK_PRINT" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_DBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_DBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_DBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_GP_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_HP0_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_HP1_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_HP2_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_HP3_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_UBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_UBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU1_UBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_DBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_DBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_DBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_GP_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_HP0_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_HP1_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_HP2_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_HP3_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_UBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_UBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU2_UBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_DBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_DBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_DBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_GP_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_HP0_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_HP1_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_HP2_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_HP3_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_UBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_UBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DPU3_UBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DSP48_VER" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DWCV_ALU_MODE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DWCV_PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "DWCV_RELU6" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ELEW_MULT_EN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "ELEW_PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GIT_COMMIT_ID" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GIT_COMMIT_TIME" -parent ${Page_0}
  ipgui::add_param $IPINST -name "GP_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HP0_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HP1_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HP2_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HP3_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "HP_DATA_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LOAD_AUGM" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LOAD_IMG_MEAN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "LOAD_PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "MISC_WR_PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "M_AXI_AWRLEN_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "M_AXI_AWRLOCK_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "M_AXI_AWRUSER_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "M_AXI_FREQ_MHZ" -parent ${Page_0}
  ipgui::add_param $IPINST -name "POOL_AVERAGE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RAM_DEPTH_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RAM_DEPTH_IMG" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RAM_DEPTH_MEAN" -parent ${Page_0}
  ipgui::add_param $IPINST -name "RAM_DEPTH_WGT" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SAVE_PARALLEL" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SFM_ENA" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SFM_HP0_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SFM_HP_DATA_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SYS_IP_TYPE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SYS_IP_VER" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SYS_REGMAP_SIZE" -parent ${Page_0}
  ipgui::add_param $IPINST -name "SYS_REGMAP_VER" -parent ${Page_0}
  ipgui::add_param $IPINST -name "S_AXI_AWRLEN_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "S_AXI_CLK_INDEPENDENT" -parent ${Page_0}
  ipgui::add_param $IPINST -name "S_AXI_FREQ_MHZ" -parent ${Page_0}
  ipgui::add_param $IPINST -name "S_AXI_ID_BW" -parent ${Page_0}
  ipgui::add_param $IPINST -name "S_AXI_SLAVES_BASE_ADDR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TIME_DAY" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TIME_HOUR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TIME_MONTH" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TIME_QUARTER" -parent ${Page_0}
  ipgui::add_param $IPINST -name "TIME_YEAR" -parent ${Page_0}
  ipgui::add_param $IPINST -name "UBANK_BIAS" -parent ${Page_0}
  ipgui::add_param $IPINST -name "UBANK_IMG_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "UBANK_WGT_N" -parent ${Page_0}
  ipgui::add_param $IPINST -name "VER_CHIP_PART" -parent ${Page_0}
  ipgui::add_param $IPINST -name "VER_DPU_NUM" -parent ${Page_0}
  ipgui::add_param $IPINST -name "VER_IP_REV" -parent ${Page_0}
  ipgui::add_param $IPINST -name "VER_TARGET" -parent ${Page_0}


}

proc update_PARAM_VALUE.ARCH_DATA_BW { PARAM_VALUE.ARCH_DATA_BW } {
	# Procedure called to update ARCH_DATA_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ARCH_DATA_BW { PARAM_VALUE.ARCH_DATA_BW } {
	# Procedure called to validate ARCH_DATA_BW
	return true
}

proc update_PARAM_VALUE.ARCH_HP_BW { PARAM_VALUE.ARCH_HP_BW } {
	# Procedure called to update ARCH_HP_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ARCH_HP_BW { PARAM_VALUE.ARCH_HP_BW } {
	# Procedure called to validate ARCH_HP_BW
	return true
}

proc update_PARAM_VALUE.ARCH_ICP { PARAM_VALUE.ARCH_ICP } {
	# Procedure called to update ARCH_ICP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ARCH_ICP { PARAM_VALUE.ARCH_ICP } {
	# Procedure called to validate ARCH_ICP
	return true
}

proc update_PARAM_VALUE.ARCH_IMG_BKGRP { PARAM_VALUE.ARCH_IMG_BKGRP } {
	# Procedure called to update ARCH_IMG_BKGRP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ARCH_IMG_BKGRP { PARAM_VALUE.ARCH_IMG_BKGRP } {
	# Procedure called to validate ARCH_IMG_BKGRP
	return true
}

proc update_PARAM_VALUE.ARCH_OCP { PARAM_VALUE.ARCH_OCP } {
	# Procedure called to update ARCH_OCP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ARCH_OCP { PARAM_VALUE.ARCH_OCP } {
	# Procedure called to validate ARCH_OCP
	return true
}

proc update_PARAM_VALUE.ARCH_PP { PARAM_VALUE.ARCH_PP } {
	# Procedure called to update ARCH_PP when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ARCH_PP { PARAM_VALUE.ARCH_PP } {
	# Procedure called to validate ARCH_PP
	return true
}

proc update_PARAM_VALUE.CLK_GATING_ENA { PARAM_VALUE.CLK_GATING_ENA } {
	# Procedure called to update CLK_GATING_ENA when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CLK_GATING_ENA { PARAM_VALUE.CLK_GATING_ENA } {
	# Procedure called to validate CLK_GATING_ENA
	return true
}

proc update_PARAM_VALUE.CONV_DSP_ACCU_ENA { PARAM_VALUE.CONV_DSP_ACCU_ENA } {
	# Procedure called to update CONV_DSP_ACCU_ENA when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CONV_DSP_ACCU_ENA { PARAM_VALUE.CONV_DSP_ACCU_ENA } {
	# Procedure called to validate CONV_DSP_ACCU_ENA
	return true
}

proc update_PARAM_VALUE.CONV_DSP_CASC_MAX { PARAM_VALUE.CONV_DSP_CASC_MAX } {
	# Procedure called to update CONV_DSP_CASC_MAX when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CONV_DSP_CASC_MAX { PARAM_VALUE.CONV_DSP_CASC_MAX } {
	# Procedure called to validate CONV_DSP_CASC_MAX
	return true
}

proc update_PARAM_VALUE.CONV_LEAKYRELU { PARAM_VALUE.CONV_LEAKYRELU } {
	# Procedure called to update CONV_LEAKYRELU when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CONV_LEAKYRELU { PARAM_VALUE.CONV_LEAKYRELU } {
	# Procedure called to validate CONV_LEAKYRELU
	return true
}

proc update_PARAM_VALUE.CONV_RELU6 { PARAM_VALUE.CONV_RELU6 } {
	# Procedure called to update CONV_RELU6 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CONV_RELU6 { PARAM_VALUE.CONV_RELU6 } {
	# Procedure called to validate CONV_RELU6
	return true
}

proc update_PARAM_VALUE.CONV_WR_PARALLEL { PARAM_VALUE.CONV_WR_PARALLEL } {
	# Procedure called to update CONV_WR_PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.CONV_WR_PARALLEL { PARAM_VALUE.CONV_WR_PARALLEL } {
	# Procedure called to validate CONV_WR_PARALLEL
	return true
}

proc update_PARAM_VALUE.DBANK_BIAS { PARAM_VALUE.DBANK_BIAS } {
	# Procedure called to update DBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DBANK_BIAS { PARAM_VALUE.DBANK_BIAS } {
	# Procedure called to validate DBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DBANK_IMG_N { PARAM_VALUE.DBANK_IMG_N } {
	# Procedure called to update DBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DBANK_IMG_N { PARAM_VALUE.DBANK_IMG_N } {
	# Procedure called to validate DBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DBANK_WGT_N { PARAM_VALUE.DBANK_WGT_N } {
	# Procedure called to update DBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DBANK_WGT_N { PARAM_VALUE.DBANK_WGT_N } {
	# Procedure called to validate DBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DNNDK_PRINT { PARAM_VALUE.DNNDK_PRINT } {
	# Procedure called to update DNNDK_PRINT when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DNNDK_PRINT { PARAM_VALUE.DNNDK_PRINT } {
	# Procedure called to validate DNNDK_PRINT
	return true
}

proc update_PARAM_VALUE.DPU1_DBANK_BIAS { PARAM_VALUE.DPU1_DBANK_BIAS } {
	# Procedure called to update DPU1_DBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_DBANK_BIAS { PARAM_VALUE.DPU1_DBANK_BIAS } {
	# Procedure called to validate DPU1_DBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DPU1_DBANK_IMG_N { PARAM_VALUE.DPU1_DBANK_IMG_N } {
	# Procedure called to update DPU1_DBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_DBANK_IMG_N { PARAM_VALUE.DPU1_DBANK_IMG_N } {
	# Procedure called to validate DPU1_DBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DPU1_DBANK_WGT_N { PARAM_VALUE.DPU1_DBANK_WGT_N } {
	# Procedure called to update DPU1_DBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_DBANK_WGT_N { PARAM_VALUE.DPU1_DBANK_WGT_N } {
	# Procedure called to validate DPU1_DBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DPU1_GP_ID_BW { PARAM_VALUE.DPU1_GP_ID_BW } {
	# Procedure called to update DPU1_GP_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_GP_ID_BW { PARAM_VALUE.DPU1_GP_ID_BW } {
	# Procedure called to validate DPU1_GP_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU1_HP0_ID_BW { PARAM_VALUE.DPU1_HP0_ID_BW } {
	# Procedure called to update DPU1_HP0_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_HP0_ID_BW { PARAM_VALUE.DPU1_HP0_ID_BW } {
	# Procedure called to validate DPU1_HP0_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU1_HP1_ID_BW { PARAM_VALUE.DPU1_HP1_ID_BW } {
	# Procedure called to update DPU1_HP1_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_HP1_ID_BW { PARAM_VALUE.DPU1_HP1_ID_BW } {
	# Procedure called to validate DPU1_HP1_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU1_HP2_ID_BW { PARAM_VALUE.DPU1_HP2_ID_BW } {
	# Procedure called to update DPU1_HP2_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_HP2_ID_BW { PARAM_VALUE.DPU1_HP2_ID_BW } {
	# Procedure called to validate DPU1_HP2_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU1_HP3_ID_BW { PARAM_VALUE.DPU1_HP3_ID_BW } {
	# Procedure called to update DPU1_HP3_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_HP3_ID_BW { PARAM_VALUE.DPU1_HP3_ID_BW } {
	# Procedure called to validate DPU1_HP3_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU1_UBANK_BIAS { PARAM_VALUE.DPU1_UBANK_BIAS } {
	# Procedure called to update DPU1_UBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_UBANK_BIAS { PARAM_VALUE.DPU1_UBANK_BIAS } {
	# Procedure called to validate DPU1_UBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DPU1_UBANK_IMG_N { PARAM_VALUE.DPU1_UBANK_IMG_N } {
	# Procedure called to update DPU1_UBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_UBANK_IMG_N { PARAM_VALUE.DPU1_UBANK_IMG_N } {
	# Procedure called to validate DPU1_UBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DPU1_UBANK_WGT_N { PARAM_VALUE.DPU1_UBANK_WGT_N } {
	# Procedure called to update DPU1_UBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU1_UBANK_WGT_N { PARAM_VALUE.DPU1_UBANK_WGT_N } {
	# Procedure called to validate DPU1_UBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DPU2_DBANK_BIAS { PARAM_VALUE.DPU2_DBANK_BIAS } {
	# Procedure called to update DPU2_DBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_DBANK_BIAS { PARAM_VALUE.DPU2_DBANK_BIAS } {
	# Procedure called to validate DPU2_DBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DPU2_DBANK_IMG_N { PARAM_VALUE.DPU2_DBANK_IMG_N } {
	# Procedure called to update DPU2_DBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_DBANK_IMG_N { PARAM_VALUE.DPU2_DBANK_IMG_N } {
	# Procedure called to validate DPU2_DBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DPU2_DBANK_WGT_N { PARAM_VALUE.DPU2_DBANK_WGT_N } {
	# Procedure called to update DPU2_DBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_DBANK_WGT_N { PARAM_VALUE.DPU2_DBANK_WGT_N } {
	# Procedure called to validate DPU2_DBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DPU2_GP_ID_BW { PARAM_VALUE.DPU2_GP_ID_BW } {
	# Procedure called to update DPU2_GP_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_GP_ID_BW { PARAM_VALUE.DPU2_GP_ID_BW } {
	# Procedure called to validate DPU2_GP_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU2_HP0_ID_BW { PARAM_VALUE.DPU2_HP0_ID_BW } {
	# Procedure called to update DPU2_HP0_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_HP0_ID_BW { PARAM_VALUE.DPU2_HP0_ID_BW } {
	# Procedure called to validate DPU2_HP0_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU2_HP1_ID_BW { PARAM_VALUE.DPU2_HP1_ID_BW } {
	# Procedure called to update DPU2_HP1_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_HP1_ID_BW { PARAM_VALUE.DPU2_HP1_ID_BW } {
	# Procedure called to validate DPU2_HP1_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU2_HP2_ID_BW { PARAM_VALUE.DPU2_HP2_ID_BW } {
	# Procedure called to update DPU2_HP2_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_HP2_ID_BW { PARAM_VALUE.DPU2_HP2_ID_BW } {
	# Procedure called to validate DPU2_HP2_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU2_HP3_ID_BW { PARAM_VALUE.DPU2_HP3_ID_BW } {
	# Procedure called to update DPU2_HP3_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_HP3_ID_BW { PARAM_VALUE.DPU2_HP3_ID_BW } {
	# Procedure called to validate DPU2_HP3_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU2_UBANK_BIAS { PARAM_VALUE.DPU2_UBANK_BIAS } {
	# Procedure called to update DPU2_UBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_UBANK_BIAS { PARAM_VALUE.DPU2_UBANK_BIAS } {
	# Procedure called to validate DPU2_UBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DPU2_UBANK_IMG_N { PARAM_VALUE.DPU2_UBANK_IMG_N } {
	# Procedure called to update DPU2_UBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_UBANK_IMG_N { PARAM_VALUE.DPU2_UBANK_IMG_N } {
	# Procedure called to validate DPU2_UBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DPU2_UBANK_WGT_N { PARAM_VALUE.DPU2_UBANK_WGT_N } {
	# Procedure called to update DPU2_UBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU2_UBANK_WGT_N { PARAM_VALUE.DPU2_UBANK_WGT_N } {
	# Procedure called to validate DPU2_UBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DPU3_DBANK_BIAS { PARAM_VALUE.DPU3_DBANK_BIAS } {
	# Procedure called to update DPU3_DBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_DBANK_BIAS { PARAM_VALUE.DPU3_DBANK_BIAS } {
	# Procedure called to validate DPU3_DBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DPU3_DBANK_IMG_N { PARAM_VALUE.DPU3_DBANK_IMG_N } {
	# Procedure called to update DPU3_DBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_DBANK_IMG_N { PARAM_VALUE.DPU3_DBANK_IMG_N } {
	# Procedure called to validate DPU3_DBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DPU3_DBANK_WGT_N { PARAM_VALUE.DPU3_DBANK_WGT_N } {
	# Procedure called to update DPU3_DBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_DBANK_WGT_N { PARAM_VALUE.DPU3_DBANK_WGT_N } {
	# Procedure called to validate DPU3_DBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DPU3_GP_ID_BW { PARAM_VALUE.DPU3_GP_ID_BW } {
	# Procedure called to update DPU3_GP_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_GP_ID_BW { PARAM_VALUE.DPU3_GP_ID_BW } {
	# Procedure called to validate DPU3_GP_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU3_HP0_ID_BW { PARAM_VALUE.DPU3_HP0_ID_BW } {
	# Procedure called to update DPU3_HP0_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_HP0_ID_BW { PARAM_VALUE.DPU3_HP0_ID_BW } {
	# Procedure called to validate DPU3_HP0_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU3_HP1_ID_BW { PARAM_VALUE.DPU3_HP1_ID_BW } {
	# Procedure called to update DPU3_HP1_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_HP1_ID_BW { PARAM_VALUE.DPU3_HP1_ID_BW } {
	# Procedure called to validate DPU3_HP1_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU3_HP2_ID_BW { PARAM_VALUE.DPU3_HP2_ID_BW } {
	# Procedure called to update DPU3_HP2_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_HP2_ID_BW { PARAM_VALUE.DPU3_HP2_ID_BW } {
	# Procedure called to validate DPU3_HP2_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU3_HP3_ID_BW { PARAM_VALUE.DPU3_HP3_ID_BW } {
	# Procedure called to update DPU3_HP3_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_HP3_ID_BW { PARAM_VALUE.DPU3_HP3_ID_BW } {
	# Procedure called to validate DPU3_HP3_ID_BW
	return true
}

proc update_PARAM_VALUE.DPU3_UBANK_BIAS { PARAM_VALUE.DPU3_UBANK_BIAS } {
	# Procedure called to update DPU3_UBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_UBANK_BIAS { PARAM_VALUE.DPU3_UBANK_BIAS } {
	# Procedure called to validate DPU3_UBANK_BIAS
	return true
}

proc update_PARAM_VALUE.DPU3_UBANK_IMG_N { PARAM_VALUE.DPU3_UBANK_IMG_N } {
	# Procedure called to update DPU3_UBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_UBANK_IMG_N { PARAM_VALUE.DPU3_UBANK_IMG_N } {
	# Procedure called to validate DPU3_UBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.DPU3_UBANK_WGT_N { PARAM_VALUE.DPU3_UBANK_WGT_N } {
	# Procedure called to update DPU3_UBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DPU3_UBANK_WGT_N { PARAM_VALUE.DPU3_UBANK_WGT_N } {
	# Procedure called to validate DPU3_UBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.DSP48_VER { PARAM_VALUE.DSP48_VER } {
	# Procedure called to update DSP48_VER when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DSP48_VER { PARAM_VALUE.DSP48_VER } {
	# Procedure called to validate DSP48_VER
	return true
}

proc update_PARAM_VALUE.DWCV_ALU_MODE { PARAM_VALUE.DWCV_ALU_MODE } {
	# Procedure called to update DWCV_ALU_MODE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DWCV_ALU_MODE { PARAM_VALUE.DWCV_ALU_MODE } {
	# Procedure called to validate DWCV_ALU_MODE
	return true
}

proc update_PARAM_VALUE.DWCV_PARALLEL { PARAM_VALUE.DWCV_PARALLEL } {
	# Procedure called to update DWCV_PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DWCV_PARALLEL { PARAM_VALUE.DWCV_PARALLEL } {
	# Procedure called to validate DWCV_PARALLEL
	return true
}

proc update_PARAM_VALUE.DWCV_RELU6 { PARAM_VALUE.DWCV_RELU6 } {
	# Procedure called to update DWCV_RELU6 when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.DWCV_RELU6 { PARAM_VALUE.DWCV_RELU6 } {
	# Procedure called to validate DWCV_RELU6
	return true
}

proc update_PARAM_VALUE.ELEW_MULT_EN { PARAM_VALUE.ELEW_MULT_EN } {
	# Procedure called to update ELEW_MULT_EN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ELEW_MULT_EN { PARAM_VALUE.ELEW_MULT_EN } {
	# Procedure called to validate ELEW_MULT_EN
	return true
}

proc update_PARAM_VALUE.ELEW_PARALLEL { PARAM_VALUE.ELEW_PARALLEL } {
	# Procedure called to update ELEW_PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.ELEW_PARALLEL { PARAM_VALUE.ELEW_PARALLEL } {
	# Procedure called to validate ELEW_PARALLEL
	return true
}

proc update_PARAM_VALUE.GIT_COMMIT_ID { PARAM_VALUE.GIT_COMMIT_ID } {
	# Procedure called to update GIT_COMMIT_ID when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GIT_COMMIT_ID { PARAM_VALUE.GIT_COMMIT_ID } {
	# Procedure called to validate GIT_COMMIT_ID
	return true
}

proc update_PARAM_VALUE.GIT_COMMIT_TIME { PARAM_VALUE.GIT_COMMIT_TIME } {
	# Procedure called to update GIT_COMMIT_TIME when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GIT_COMMIT_TIME { PARAM_VALUE.GIT_COMMIT_TIME } {
	# Procedure called to validate GIT_COMMIT_TIME
	return true
}

proc update_PARAM_VALUE.GP_ID_BW { PARAM_VALUE.GP_ID_BW } {
	# Procedure called to update GP_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.GP_ID_BW { PARAM_VALUE.GP_ID_BW } {
	# Procedure called to validate GP_ID_BW
	return true
}

proc update_PARAM_VALUE.HP0_ID_BW { PARAM_VALUE.HP0_ID_BW } {
	# Procedure called to update HP0_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HP0_ID_BW { PARAM_VALUE.HP0_ID_BW } {
	# Procedure called to validate HP0_ID_BW
	return true
}

proc update_PARAM_VALUE.HP1_ID_BW { PARAM_VALUE.HP1_ID_BW } {
	# Procedure called to update HP1_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HP1_ID_BW { PARAM_VALUE.HP1_ID_BW } {
	# Procedure called to validate HP1_ID_BW
	return true
}

proc update_PARAM_VALUE.HP2_ID_BW { PARAM_VALUE.HP2_ID_BW } {
	# Procedure called to update HP2_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HP2_ID_BW { PARAM_VALUE.HP2_ID_BW } {
	# Procedure called to validate HP2_ID_BW
	return true
}

proc update_PARAM_VALUE.HP3_ID_BW { PARAM_VALUE.HP3_ID_BW } {
	# Procedure called to update HP3_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HP3_ID_BW { PARAM_VALUE.HP3_ID_BW } {
	# Procedure called to validate HP3_ID_BW
	return true
}

proc update_PARAM_VALUE.HP_DATA_BW { PARAM_VALUE.HP_DATA_BW } {
	# Procedure called to update HP_DATA_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.HP_DATA_BW { PARAM_VALUE.HP_DATA_BW } {
	# Procedure called to validate HP_DATA_BW
	return true
}

proc update_PARAM_VALUE.LOAD_AUGM { PARAM_VALUE.LOAD_AUGM } {
	# Procedure called to update LOAD_AUGM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOAD_AUGM { PARAM_VALUE.LOAD_AUGM } {
	# Procedure called to validate LOAD_AUGM
	return true
}

proc update_PARAM_VALUE.LOAD_IMG_MEAN { PARAM_VALUE.LOAD_IMG_MEAN } {
	# Procedure called to update LOAD_IMG_MEAN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOAD_IMG_MEAN { PARAM_VALUE.LOAD_IMG_MEAN } {
	# Procedure called to validate LOAD_IMG_MEAN
	return true
}

proc update_PARAM_VALUE.LOAD_PARALLEL { PARAM_VALUE.LOAD_PARALLEL } {
	# Procedure called to update LOAD_PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.LOAD_PARALLEL { PARAM_VALUE.LOAD_PARALLEL } {
	# Procedure called to validate LOAD_PARALLEL
	return true
}

proc update_PARAM_VALUE.MISC_WR_PARALLEL { PARAM_VALUE.MISC_WR_PARALLEL } {
	# Procedure called to update MISC_WR_PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.MISC_WR_PARALLEL { PARAM_VALUE.MISC_WR_PARALLEL } {
	# Procedure called to validate MISC_WR_PARALLEL
	return true
}

proc update_PARAM_VALUE.M_AXI_AWRLEN_BW { PARAM_VALUE.M_AXI_AWRLEN_BW } {
	# Procedure called to update M_AXI_AWRLEN_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.M_AXI_AWRLEN_BW { PARAM_VALUE.M_AXI_AWRLEN_BW } {
	# Procedure called to validate M_AXI_AWRLEN_BW
	return true
}

proc update_PARAM_VALUE.M_AXI_AWRLOCK_BW { PARAM_VALUE.M_AXI_AWRLOCK_BW } {
	# Procedure called to update M_AXI_AWRLOCK_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.M_AXI_AWRLOCK_BW { PARAM_VALUE.M_AXI_AWRLOCK_BW } {
	# Procedure called to validate M_AXI_AWRLOCK_BW
	return true
}

proc update_PARAM_VALUE.M_AXI_AWRUSER_BW { PARAM_VALUE.M_AXI_AWRUSER_BW } {
	# Procedure called to update M_AXI_AWRUSER_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.M_AXI_AWRUSER_BW { PARAM_VALUE.M_AXI_AWRUSER_BW } {
	# Procedure called to validate M_AXI_AWRUSER_BW
	return true
}

proc update_PARAM_VALUE.M_AXI_FREQ_MHZ { PARAM_VALUE.M_AXI_FREQ_MHZ } {
	# Procedure called to update M_AXI_FREQ_MHZ when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.M_AXI_FREQ_MHZ { PARAM_VALUE.M_AXI_FREQ_MHZ } {
	# Procedure called to validate M_AXI_FREQ_MHZ
	return true
}

proc update_PARAM_VALUE.POOL_AVERAGE { PARAM_VALUE.POOL_AVERAGE } {
	# Procedure called to update POOL_AVERAGE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.POOL_AVERAGE { PARAM_VALUE.POOL_AVERAGE } {
	# Procedure called to validate POOL_AVERAGE
	return true
}

proc update_PARAM_VALUE.RAM_DEPTH_BIAS { PARAM_VALUE.RAM_DEPTH_BIAS } {
	# Procedure called to update RAM_DEPTH_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RAM_DEPTH_BIAS { PARAM_VALUE.RAM_DEPTH_BIAS } {
	# Procedure called to validate RAM_DEPTH_BIAS
	return true
}

proc update_PARAM_VALUE.RAM_DEPTH_IMG { PARAM_VALUE.RAM_DEPTH_IMG } {
	# Procedure called to update RAM_DEPTH_IMG when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RAM_DEPTH_IMG { PARAM_VALUE.RAM_DEPTH_IMG } {
	# Procedure called to validate RAM_DEPTH_IMG
	return true
}

proc update_PARAM_VALUE.RAM_DEPTH_MEAN { PARAM_VALUE.RAM_DEPTH_MEAN } {
	# Procedure called to update RAM_DEPTH_MEAN when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RAM_DEPTH_MEAN { PARAM_VALUE.RAM_DEPTH_MEAN } {
	# Procedure called to validate RAM_DEPTH_MEAN
	return true
}

proc update_PARAM_VALUE.RAM_DEPTH_WGT { PARAM_VALUE.RAM_DEPTH_WGT } {
	# Procedure called to update RAM_DEPTH_WGT when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.RAM_DEPTH_WGT { PARAM_VALUE.RAM_DEPTH_WGT } {
	# Procedure called to validate RAM_DEPTH_WGT
	return true
}

proc update_PARAM_VALUE.SAVE_PARALLEL { PARAM_VALUE.SAVE_PARALLEL } {
	# Procedure called to update SAVE_PARALLEL when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SAVE_PARALLEL { PARAM_VALUE.SAVE_PARALLEL } {
	# Procedure called to validate SAVE_PARALLEL
	return true
}

proc update_PARAM_VALUE.SFM_ENA { PARAM_VALUE.SFM_ENA } {
	# Procedure called to update SFM_ENA when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SFM_ENA { PARAM_VALUE.SFM_ENA } {
	# Procedure called to validate SFM_ENA
	return true
}

proc update_PARAM_VALUE.SFM_HP0_ID_BW { PARAM_VALUE.SFM_HP0_ID_BW } {
	# Procedure called to update SFM_HP0_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SFM_HP0_ID_BW { PARAM_VALUE.SFM_HP0_ID_BW } {
	# Procedure called to validate SFM_HP0_ID_BW
	return true
}

proc update_PARAM_VALUE.SFM_HP_DATA_BW { PARAM_VALUE.SFM_HP_DATA_BW } {
	# Procedure called to update SFM_HP_DATA_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SFM_HP_DATA_BW { PARAM_VALUE.SFM_HP_DATA_BW } {
	# Procedure called to validate SFM_HP_DATA_BW
	return true
}

proc update_PARAM_VALUE.SYS_IP_TYPE { PARAM_VALUE.SYS_IP_TYPE } {
	# Procedure called to update SYS_IP_TYPE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SYS_IP_TYPE { PARAM_VALUE.SYS_IP_TYPE } {
	# Procedure called to validate SYS_IP_TYPE
	return true
}

proc update_PARAM_VALUE.SYS_IP_VER { PARAM_VALUE.SYS_IP_VER } {
	# Procedure called to update SYS_IP_VER when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SYS_IP_VER { PARAM_VALUE.SYS_IP_VER } {
	# Procedure called to validate SYS_IP_VER
	return true
}

proc update_PARAM_VALUE.SYS_REGMAP_SIZE { PARAM_VALUE.SYS_REGMAP_SIZE } {
	# Procedure called to update SYS_REGMAP_SIZE when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SYS_REGMAP_SIZE { PARAM_VALUE.SYS_REGMAP_SIZE } {
	# Procedure called to validate SYS_REGMAP_SIZE
	return true
}

proc update_PARAM_VALUE.SYS_REGMAP_VER { PARAM_VALUE.SYS_REGMAP_VER } {
	# Procedure called to update SYS_REGMAP_VER when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SYS_REGMAP_VER { PARAM_VALUE.SYS_REGMAP_VER } {
	# Procedure called to validate SYS_REGMAP_VER
	return true
}

proc update_PARAM_VALUE.S_AXI_AWRLEN_BW { PARAM_VALUE.S_AXI_AWRLEN_BW } {
	# Procedure called to update S_AXI_AWRLEN_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.S_AXI_AWRLEN_BW { PARAM_VALUE.S_AXI_AWRLEN_BW } {
	# Procedure called to validate S_AXI_AWRLEN_BW
	return true
}

proc update_PARAM_VALUE.S_AXI_CLK_INDEPENDENT { PARAM_VALUE.S_AXI_CLK_INDEPENDENT } {
	# Procedure called to update S_AXI_CLK_INDEPENDENT when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.S_AXI_CLK_INDEPENDENT { PARAM_VALUE.S_AXI_CLK_INDEPENDENT } {
	# Procedure called to validate S_AXI_CLK_INDEPENDENT
	return true
}

proc update_PARAM_VALUE.S_AXI_FREQ_MHZ { PARAM_VALUE.S_AXI_FREQ_MHZ } {
	# Procedure called to update S_AXI_FREQ_MHZ when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.S_AXI_FREQ_MHZ { PARAM_VALUE.S_AXI_FREQ_MHZ } {
	# Procedure called to validate S_AXI_FREQ_MHZ
	return true
}

proc update_PARAM_VALUE.S_AXI_ID_BW { PARAM_VALUE.S_AXI_ID_BW } {
	# Procedure called to update S_AXI_ID_BW when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.S_AXI_ID_BW { PARAM_VALUE.S_AXI_ID_BW } {
	# Procedure called to validate S_AXI_ID_BW
	return true
}

proc update_PARAM_VALUE.S_AXI_SLAVES_BASE_ADDR { PARAM_VALUE.S_AXI_SLAVES_BASE_ADDR } {
	# Procedure called to update S_AXI_SLAVES_BASE_ADDR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.S_AXI_SLAVES_BASE_ADDR { PARAM_VALUE.S_AXI_SLAVES_BASE_ADDR } {
	# Procedure called to validate S_AXI_SLAVES_BASE_ADDR
	return true
}

proc update_PARAM_VALUE.TIME_DAY { PARAM_VALUE.TIME_DAY } {
	# Procedure called to update TIME_DAY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TIME_DAY { PARAM_VALUE.TIME_DAY } {
	# Procedure called to validate TIME_DAY
	return true
}

proc update_PARAM_VALUE.TIME_HOUR { PARAM_VALUE.TIME_HOUR } {
	# Procedure called to update TIME_HOUR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TIME_HOUR { PARAM_VALUE.TIME_HOUR } {
	# Procedure called to validate TIME_HOUR
	return true
}

proc update_PARAM_VALUE.TIME_MONTH { PARAM_VALUE.TIME_MONTH } {
	# Procedure called to update TIME_MONTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TIME_MONTH { PARAM_VALUE.TIME_MONTH } {
	# Procedure called to validate TIME_MONTH
	return true
}

proc update_PARAM_VALUE.TIME_QUARTER { PARAM_VALUE.TIME_QUARTER } {
	# Procedure called to update TIME_QUARTER when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TIME_QUARTER { PARAM_VALUE.TIME_QUARTER } {
	# Procedure called to validate TIME_QUARTER
	return true
}

proc update_PARAM_VALUE.TIME_YEAR { PARAM_VALUE.TIME_YEAR } {
	# Procedure called to update TIME_YEAR when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.TIME_YEAR { PARAM_VALUE.TIME_YEAR } {
	# Procedure called to validate TIME_YEAR
	return true
}

proc update_PARAM_VALUE.UBANK_BIAS { PARAM_VALUE.UBANK_BIAS } {
	# Procedure called to update UBANK_BIAS when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.UBANK_BIAS { PARAM_VALUE.UBANK_BIAS } {
	# Procedure called to validate UBANK_BIAS
	return true
}

proc update_PARAM_VALUE.UBANK_IMG_N { PARAM_VALUE.UBANK_IMG_N } {
	# Procedure called to update UBANK_IMG_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.UBANK_IMG_N { PARAM_VALUE.UBANK_IMG_N } {
	# Procedure called to validate UBANK_IMG_N
	return true
}

proc update_PARAM_VALUE.UBANK_WGT_N { PARAM_VALUE.UBANK_WGT_N } {
	# Procedure called to update UBANK_WGT_N when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.UBANK_WGT_N { PARAM_VALUE.UBANK_WGT_N } {
	# Procedure called to validate UBANK_WGT_N
	return true
}

proc update_PARAM_VALUE.VER_CHIP_PART { PARAM_VALUE.VER_CHIP_PART } {
	# Procedure called to update VER_CHIP_PART when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VER_CHIP_PART { PARAM_VALUE.VER_CHIP_PART } {
	# Procedure called to validate VER_CHIP_PART
	return true
}

proc update_PARAM_VALUE.VER_DPU_NUM { PARAM_VALUE.VER_DPU_NUM } {
	# Procedure called to update VER_DPU_NUM when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VER_DPU_NUM { PARAM_VALUE.VER_DPU_NUM } {
	# Procedure called to validate VER_DPU_NUM
	return true
}

proc update_PARAM_VALUE.VER_IP_REV { PARAM_VALUE.VER_IP_REV } {
	# Procedure called to update VER_IP_REV when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VER_IP_REV { PARAM_VALUE.VER_IP_REV } {
	# Procedure called to validate VER_IP_REV
	return true
}

proc update_PARAM_VALUE.VER_TARGET { PARAM_VALUE.VER_TARGET } {
	# Procedure called to update VER_TARGET when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.VER_TARGET { PARAM_VALUE.VER_TARGET } {
	# Procedure called to validate VER_TARGET
	return true
}


proc update_MODELPARAM_VALUE.VER_CHIP_PART { MODELPARAM_VALUE.VER_CHIP_PART PARAM_VALUE.VER_CHIP_PART } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VER_CHIP_PART}] ${MODELPARAM_VALUE.VER_CHIP_PART}
}

proc update_MODELPARAM_VALUE.VER_DPU_NUM { MODELPARAM_VALUE.VER_DPU_NUM PARAM_VALUE.VER_DPU_NUM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VER_DPU_NUM}] ${MODELPARAM_VALUE.VER_DPU_NUM}
}

proc update_MODELPARAM_VALUE.CLK_GATING_ENA { MODELPARAM_VALUE.CLK_GATING_ENA PARAM_VALUE.CLK_GATING_ENA } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CLK_GATING_ENA}] ${MODELPARAM_VALUE.CLK_GATING_ENA}
}

proc update_MODELPARAM_VALUE.DSP48_VER { MODELPARAM_VALUE.DSP48_VER PARAM_VALUE.DSP48_VER } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DSP48_VER}] ${MODELPARAM_VALUE.DSP48_VER}
}

proc update_MODELPARAM_VALUE.S_AXI_FREQ_MHZ { MODELPARAM_VALUE.S_AXI_FREQ_MHZ PARAM_VALUE.S_AXI_FREQ_MHZ } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.S_AXI_FREQ_MHZ}] ${MODELPARAM_VALUE.S_AXI_FREQ_MHZ}
}

proc update_MODELPARAM_VALUE.S_AXI_CLK_INDEPENDENT { MODELPARAM_VALUE.S_AXI_CLK_INDEPENDENT PARAM_VALUE.S_AXI_CLK_INDEPENDENT } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.S_AXI_CLK_INDEPENDENT}] ${MODELPARAM_VALUE.S_AXI_CLK_INDEPENDENT}
}

proc update_MODELPARAM_VALUE.S_AXI_SLAVES_BASE_ADDR { MODELPARAM_VALUE.S_AXI_SLAVES_BASE_ADDR PARAM_VALUE.S_AXI_SLAVES_BASE_ADDR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.S_AXI_SLAVES_BASE_ADDR}] ${MODELPARAM_VALUE.S_AXI_SLAVES_BASE_ADDR}
}

proc update_MODELPARAM_VALUE.S_AXI_ID_BW { MODELPARAM_VALUE.S_AXI_ID_BW PARAM_VALUE.S_AXI_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.S_AXI_ID_BW}] ${MODELPARAM_VALUE.S_AXI_ID_BW}
}

proc update_MODELPARAM_VALUE.S_AXI_AWRLEN_BW { MODELPARAM_VALUE.S_AXI_AWRLEN_BW PARAM_VALUE.S_AXI_AWRLEN_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.S_AXI_AWRLEN_BW}] ${MODELPARAM_VALUE.S_AXI_AWRLEN_BW}
}

proc update_MODELPARAM_VALUE.M_AXI_FREQ_MHZ { MODELPARAM_VALUE.M_AXI_FREQ_MHZ PARAM_VALUE.M_AXI_FREQ_MHZ } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.M_AXI_FREQ_MHZ}] ${MODELPARAM_VALUE.M_AXI_FREQ_MHZ}
}

proc update_MODELPARAM_VALUE.M_AXI_AWRLEN_BW { MODELPARAM_VALUE.M_AXI_AWRLEN_BW PARAM_VALUE.M_AXI_AWRLEN_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.M_AXI_AWRLEN_BW}] ${MODELPARAM_VALUE.M_AXI_AWRLEN_BW}
}

proc update_MODELPARAM_VALUE.M_AXI_AWRUSER_BW { MODELPARAM_VALUE.M_AXI_AWRUSER_BW PARAM_VALUE.M_AXI_AWRUSER_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.M_AXI_AWRUSER_BW}] ${MODELPARAM_VALUE.M_AXI_AWRUSER_BW}
}

proc update_MODELPARAM_VALUE.M_AXI_AWRLOCK_BW { MODELPARAM_VALUE.M_AXI_AWRLOCK_BW PARAM_VALUE.M_AXI_AWRLOCK_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.M_AXI_AWRLOCK_BW}] ${MODELPARAM_VALUE.M_AXI_AWRLOCK_BW}
}

proc update_MODELPARAM_VALUE.GP_ID_BW { MODELPARAM_VALUE.GP_ID_BW PARAM_VALUE.GP_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GP_ID_BW}] ${MODELPARAM_VALUE.GP_ID_BW}
}

proc update_MODELPARAM_VALUE.HP0_ID_BW { MODELPARAM_VALUE.HP0_ID_BW PARAM_VALUE.HP0_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HP0_ID_BW}] ${MODELPARAM_VALUE.HP0_ID_BW}
}

proc update_MODELPARAM_VALUE.HP1_ID_BW { MODELPARAM_VALUE.HP1_ID_BW PARAM_VALUE.HP1_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HP1_ID_BW}] ${MODELPARAM_VALUE.HP1_ID_BW}
}

proc update_MODELPARAM_VALUE.HP2_ID_BW { MODELPARAM_VALUE.HP2_ID_BW PARAM_VALUE.HP2_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HP2_ID_BW}] ${MODELPARAM_VALUE.HP2_ID_BW}
}

proc update_MODELPARAM_VALUE.HP3_ID_BW { MODELPARAM_VALUE.HP3_ID_BW PARAM_VALUE.HP3_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HP3_ID_BW}] ${MODELPARAM_VALUE.HP3_ID_BW}
}

proc update_MODELPARAM_VALUE.HP_DATA_BW { MODELPARAM_VALUE.HP_DATA_BW PARAM_VALUE.HP_DATA_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.HP_DATA_BW}] ${MODELPARAM_VALUE.HP_DATA_BW}
}

proc update_MODELPARAM_VALUE.SYS_IP_VER { MODELPARAM_VALUE.SYS_IP_VER PARAM_VALUE.SYS_IP_VER } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SYS_IP_VER}] ${MODELPARAM_VALUE.SYS_IP_VER}
}

proc update_MODELPARAM_VALUE.SYS_IP_TYPE { MODELPARAM_VALUE.SYS_IP_TYPE PARAM_VALUE.SYS_IP_TYPE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SYS_IP_TYPE}] ${MODELPARAM_VALUE.SYS_IP_TYPE}
}

proc update_MODELPARAM_VALUE.SYS_REGMAP_SIZE { MODELPARAM_VALUE.SYS_REGMAP_SIZE PARAM_VALUE.SYS_REGMAP_SIZE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SYS_REGMAP_SIZE}] ${MODELPARAM_VALUE.SYS_REGMAP_SIZE}
}

proc update_MODELPARAM_VALUE.SYS_REGMAP_VER { MODELPARAM_VALUE.SYS_REGMAP_VER PARAM_VALUE.SYS_REGMAP_VER } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SYS_REGMAP_VER}] ${MODELPARAM_VALUE.SYS_REGMAP_VER}
}

proc update_MODELPARAM_VALUE.TIME_YEAR { MODELPARAM_VALUE.TIME_YEAR PARAM_VALUE.TIME_YEAR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TIME_YEAR}] ${MODELPARAM_VALUE.TIME_YEAR}
}

proc update_MODELPARAM_VALUE.TIME_MONTH { MODELPARAM_VALUE.TIME_MONTH PARAM_VALUE.TIME_MONTH } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TIME_MONTH}] ${MODELPARAM_VALUE.TIME_MONTH}
}

proc update_MODELPARAM_VALUE.TIME_DAY { MODELPARAM_VALUE.TIME_DAY PARAM_VALUE.TIME_DAY } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TIME_DAY}] ${MODELPARAM_VALUE.TIME_DAY}
}

proc update_MODELPARAM_VALUE.TIME_HOUR { MODELPARAM_VALUE.TIME_HOUR PARAM_VALUE.TIME_HOUR } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TIME_HOUR}] ${MODELPARAM_VALUE.TIME_HOUR}
}

proc update_MODELPARAM_VALUE.TIME_QUARTER { MODELPARAM_VALUE.TIME_QUARTER PARAM_VALUE.TIME_QUARTER } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.TIME_QUARTER}] ${MODELPARAM_VALUE.TIME_QUARTER}
}

proc update_MODELPARAM_VALUE.GIT_COMMIT_ID { MODELPARAM_VALUE.GIT_COMMIT_ID PARAM_VALUE.GIT_COMMIT_ID } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GIT_COMMIT_ID}] ${MODELPARAM_VALUE.GIT_COMMIT_ID}
}

proc update_MODELPARAM_VALUE.GIT_COMMIT_TIME { MODELPARAM_VALUE.GIT_COMMIT_TIME PARAM_VALUE.GIT_COMMIT_TIME } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.GIT_COMMIT_TIME}] ${MODELPARAM_VALUE.GIT_COMMIT_TIME}
}

proc update_MODELPARAM_VALUE.VER_IP_REV { MODELPARAM_VALUE.VER_IP_REV PARAM_VALUE.VER_IP_REV } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VER_IP_REV}] ${MODELPARAM_VALUE.VER_IP_REV}
}

proc update_MODELPARAM_VALUE.VER_TARGET { MODELPARAM_VALUE.VER_TARGET PARAM_VALUE.VER_TARGET } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.VER_TARGET}] ${MODELPARAM_VALUE.VER_TARGET}
}

proc update_MODELPARAM_VALUE.ARCH_HP_BW { MODELPARAM_VALUE.ARCH_HP_BW PARAM_VALUE.ARCH_HP_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ARCH_HP_BW}] ${MODELPARAM_VALUE.ARCH_HP_BW}
}

proc update_MODELPARAM_VALUE.ARCH_DATA_BW { MODELPARAM_VALUE.ARCH_DATA_BW PARAM_VALUE.ARCH_DATA_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ARCH_DATA_BW}] ${MODELPARAM_VALUE.ARCH_DATA_BW}
}

proc update_MODELPARAM_VALUE.ARCH_IMG_BKGRP { MODELPARAM_VALUE.ARCH_IMG_BKGRP PARAM_VALUE.ARCH_IMG_BKGRP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ARCH_IMG_BKGRP}] ${MODELPARAM_VALUE.ARCH_IMG_BKGRP}
}

proc update_MODELPARAM_VALUE.ARCH_PP { MODELPARAM_VALUE.ARCH_PP PARAM_VALUE.ARCH_PP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ARCH_PP}] ${MODELPARAM_VALUE.ARCH_PP}
}

proc update_MODELPARAM_VALUE.ARCH_ICP { MODELPARAM_VALUE.ARCH_ICP PARAM_VALUE.ARCH_ICP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ARCH_ICP}] ${MODELPARAM_VALUE.ARCH_ICP}
}

proc update_MODELPARAM_VALUE.ARCH_OCP { MODELPARAM_VALUE.ARCH_OCP PARAM_VALUE.ARCH_OCP } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ARCH_OCP}] ${MODELPARAM_VALUE.ARCH_OCP}
}

proc update_MODELPARAM_VALUE.RAM_DEPTH_MEAN { MODELPARAM_VALUE.RAM_DEPTH_MEAN PARAM_VALUE.RAM_DEPTH_MEAN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RAM_DEPTH_MEAN}] ${MODELPARAM_VALUE.RAM_DEPTH_MEAN}
}

proc update_MODELPARAM_VALUE.RAM_DEPTH_BIAS { MODELPARAM_VALUE.RAM_DEPTH_BIAS PARAM_VALUE.RAM_DEPTH_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RAM_DEPTH_BIAS}] ${MODELPARAM_VALUE.RAM_DEPTH_BIAS}
}

proc update_MODELPARAM_VALUE.RAM_DEPTH_WGT { MODELPARAM_VALUE.RAM_DEPTH_WGT PARAM_VALUE.RAM_DEPTH_WGT } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RAM_DEPTH_WGT}] ${MODELPARAM_VALUE.RAM_DEPTH_WGT}
}

proc update_MODELPARAM_VALUE.RAM_DEPTH_IMG { MODELPARAM_VALUE.RAM_DEPTH_IMG PARAM_VALUE.RAM_DEPTH_IMG } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.RAM_DEPTH_IMG}] ${MODELPARAM_VALUE.RAM_DEPTH_IMG}
}

proc update_MODELPARAM_VALUE.UBANK_IMG_N { MODELPARAM_VALUE.UBANK_IMG_N PARAM_VALUE.UBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.UBANK_IMG_N}] ${MODELPARAM_VALUE.UBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.UBANK_WGT_N { MODELPARAM_VALUE.UBANK_WGT_N PARAM_VALUE.UBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.UBANK_WGT_N}] ${MODELPARAM_VALUE.UBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.UBANK_BIAS { MODELPARAM_VALUE.UBANK_BIAS PARAM_VALUE.UBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.UBANK_BIAS}] ${MODELPARAM_VALUE.UBANK_BIAS}
}

proc update_MODELPARAM_VALUE.DBANK_IMG_N { MODELPARAM_VALUE.DBANK_IMG_N PARAM_VALUE.DBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DBANK_IMG_N}] ${MODELPARAM_VALUE.DBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DBANK_WGT_N { MODELPARAM_VALUE.DBANK_WGT_N PARAM_VALUE.DBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DBANK_WGT_N}] ${MODELPARAM_VALUE.DBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DBANK_BIAS { MODELPARAM_VALUE.DBANK_BIAS PARAM_VALUE.DBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DBANK_BIAS}] ${MODELPARAM_VALUE.DBANK_BIAS}
}

proc update_MODELPARAM_VALUE.LOAD_AUGM { MODELPARAM_VALUE.LOAD_AUGM PARAM_VALUE.LOAD_AUGM } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOAD_AUGM}] ${MODELPARAM_VALUE.LOAD_AUGM}
}

proc update_MODELPARAM_VALUE.LOAD_IMG_MEAN { MODELPARAM_VALUE.LOAD_IMG_MEAN PARAM_VALUE.LOAD_IMG_MEAN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOAD_IMG_MEAN}] ${MODELPARAM_VALUE.LOAD_IMG_MEAN}
}

proc update_MODELPARAM_VALUE.LOAD_PARALLEL { MODELPARAM_VALUE.LOAD_PARALLEL PARAM_VALUE.LOAD_PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.LOAD_PARALLEL}] ${MODELPARAM_VALUE.LOAD_PARALLEL}
}

proc update_MODELPARAM_VALUE.CONV_LEAKYRELU { MODELPARAM_VALUE.CONV_LEAKYRELU PARAM_VALUE.CONV_LEAKYRELU } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CONV_LEAKYRELU}] ${MODELPARAM_VALUE.CONV_LEAKYRELU}
}

proc update_MODELPARAM_VALUE.CONV_RELU6 { MODELPARAM_VALUE.CONV_RELU6 PARAM_VALUE.CONV_RELU6 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CONV_RELU6}] ${MODELPARAM_VALUE.CONV_RELU6}
}

proc update_MODELPARAM_VALUE.CONV_WR_PARALLEL { MODELPARAM_VALUE.CONV_WR_PARALLEL PARAM_VALUE.CONV_WR_PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CONV_WR_PARALLEL}] ${MODELPARAM_VALUE.CONV_WR_PARALLEL}
}

proc update_MODELPARAM_VALUE.CONV_DSP_CASC_MAX { MODELPARAM_VALUE.CONV_DSP_CASC_MAX PARAM_VALUE.CONV_DSP_CASC_MAX } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CONV_DSP_CASC_MAX}] ${MODELPARAM_VALUE.CONV_DSP_CASC_MAX}
}

proc update_MODELPARAM_VALUE.CONV_DSP_ACCU_ENA { MODELPARAM_VALUE.CONV_DSP_ACCU_ENA PARAM_VALUE.CONV_DSP_ACCU_ENA } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.CONV_DSP_ACCU_ENA}] ${MODELPARAM_VALUE.CONV_DSP_ACCU_ENA}
}

proc update_MODELPARAM_VALUE.SAVE_PARALLEL { MODELPARAM_VALUE.SAVE_PARALLEL PARAM_VALUE.SAVE_PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SAVE_PARALLEL}] ${MODELPARAM_VALUE.SAVE_PARALLEL}
}

proc update_MODELPARAM_VALUE.POOL_AVERAGE { MODELPARAM_VALUE.POOL_AVERAGE PARAM_VALUE.POOL_AVERAGE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.POOL_AVERAGE}] ${MODELPARAM_VALUE.POOL_AVERAGE}
}

proc update_MODELPARAM_VALUE.ELEW_PARALLEL { MODELPARAM_VALUE.ELEW_PARALLEL PARAM_VALUE.ELEW_PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ELEW_PARALLEL}] ${MODELPARAM_VALUE.ELEW_PARALLEL}
}

proc update_MODELPARAM_VALUE.ELEW_MULT_EN { MODELPARAM_VALUE.ELEW_MULT_EN PARAM_VALUE.ELEW_MULT_EN } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.ELEW_MULT_EN}] ${MODELPARAM_VALUE.ELEW_MULT_EN}
}

proc update_MODELPARAM_VALUE.DWCV_ALU_MODE { MODELPARAM_VALUE.DWCV_ALU_MODE PARAM_VALUE.DWCV_ALU_MODE } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DWCV_ALU_MODE}] ${MODELPARAM_VALUE.DWCV_ALU_MODE}
}

proc update_MODELPARAM_VALUE.DWCV_RELU6 { MODELPARAM_VALUE.DWCV_RELU6 PARAM_VALUE.DWCV_RELU6 } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DWCV_RELU6}] ${MODELPARAM_VALUE.DWCV_RELU6}
}

proc update_MODELPARAM_VALUE.DWCV_PARALLEL { MODELPARAM_VALUE.DWCV_PARALLEL PARAM_VALUE.DWCV_PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DWCV_PARALLEL}] ${MODELPARAM_VALUE.DWCV_PARALLEL}
}

proc update_MODELPARAM_VALUE.MISC_WR_PARALLEL { MODELPARAM_VALUE.MISC_WR_PARALLEL PARAM_VALUE.MISC_WR_PARALLEL } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.MISC_WR_PARALLEL}] ${MODELPARAM_VALUE.MISC_WR_PARALLEL}
}

proc update_MODELPARAM_VALUE.DNNDK_PRINT { MODELPARAM_VALUE.DNNDK_PRINT PARAM_VALUE.DNNDK_PRINT } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DNNDK_PRINT}] ${MODELPARAM_VALUE.DNNDK_PRINT}
}

proc update_MODELPARAM_VALUE.DPU1_GP_ID_BW { MODELPARAM_VALUE.DPU1_GP_ID_BW PARAM_VALUE.DPU1_GP_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_GP_ID_BW}] ${MODELPARAM_VALUE.DPU1_GP_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU1_HP0_ID_BW { MODELPARAM_VALUE.DPU1_HP0_ID_BW PARAM_VALUE.DPU1_HP0_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_HP0_ID_BW}] ${MODELPARAM_VALUE.DPU1_HP0_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU1_HP1_ID_BW { MODELPARAM_VALUE.DPU1_HP1_ID_BW PARAM_VALUE.DPU1_HP1_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_HP1_ID_BW}] ${MODELPARAM_VALUE.DPU1_HP1_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU1_HP2_ID_BW { MODELPARAM_VALUE.DPU1_HP2_ID_BW PARAM_VALUE.DPU1_HP2_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_HP2_ID_BW}] ${MODELPARAM_VALUE.DPU1_HP2_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU1_HP3_ID_BW { MODELPARAM_VALUE.DPU1_HP3_ID_BW PARAM_VALUE.DPU1_HP3_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_HP3_ID_BW}] ${MODELPARAM_VALUE.DPU1_HP3_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU1_UBANK_IMG_N { MODELPARAM_VALUE.DPU1_UBANK_IMG_N PARAM_VALUE.DPU1_UBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_UBANK_IMG_N}] ${MODELPARAM_VALUE.DPU1_UBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DPU1_UBANK_WGT_N { MODELPARAM_VALUE.DPU1_UBANK_WGT_N PARAM_VALUE.DPU1_UBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_UBANK_WGT_N}] ${MODELPARAM_VALUE.DPU1_UBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DPU1_UBANK_BIAS { MODELPARAM_VALUE.DPU1_UBANK_BIAS PARAM_VALUE.DPU1_UBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_UBANK_BIAS}] ${MODELPARAM_VALUE.DPU1_UBANK_BIAS}
}

proc update_MODELPARAM_VALUE.DPU1_DBANK_IMG_N { MODELPARAM_VALUE.DPU1_DBANK_IMG_N PARAM_VALUE.DPU1_DBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_DBANK_IMG_N}] ${MODELPARAM_VALUE.DPU1_DBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DPU1_DBANK_WGT_N { MODELPARAM_VALUE.DPU1_DBANK_WGT_N PARAM_VALUE.DPU1_DBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_DBANK_WGT_N}] ${MODELPARAM_VALUE.DPU1_DBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DPU1_DBANK_BIAS { MODELPARAM_VALUE.DPU1_DBANK_BIAS PARAM_VALUE.DPU1_DBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU1_DBANK_BIAS}] ${MODELPARAM_VALUE.DPU1_DBANK_BIAS}
}

proc update_MODELPARAM_VALUE.DPU2_GP_ID_BW { MODELPARAM_VALUE.DPU2_GP_ID_BW PARAM_VALUE.DPU2_GP_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_GP_ID_BW}] ${MODELPARAM_VALUE.DPU2_GP_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU2_HP0_ID_BW { MODELPARAM_VALUE.DPU2_HP0_ID_BW PARAM_VALUE.DPU2_HP0_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_HP0_ID_BW}] ${MODELPARAM_VALUE.DPU2_HP0_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU2_HP1_ID_BW { MODELPARAM_VALUE.DPU2_HP1_ID_BW PARAM_VALUE.DPU2_HP1_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_HP1_ID_BW}] ${MODELPARAM_VALUE.DPU2_HP1_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU2_HP2_ID_BW { MODELPARAM_VALUE.DPU2_HP2_ID_BW PARAM_VALUE.DPU2_HP2_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_HP2_ID_BW}] ${MODELPARAM_VALUE.DPU2_HP2_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU2_HP3_ID_BW { MODELPARAM_VALUE.DPU2_HP3_ID_BW PARAM_VALUE.DPU2_HP3_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_HP3_ID_BW}] ${MODELPARAM_VALUE.DPU2_HP3_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU2_UBANK_IMG_N { MODELPARAM_VALUE.DPU2_UBANK_IMG_N PARAM_VALUE.DPU2_UBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_UBANK_IMG_N}] ${MODELPARAM_VALUE.DPU2_UBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DPU2_UBANK_WGT_N { MODELPARAM_VALUE.DPU2_UBANK_WGT_N PARAM_VALUE.DPU2_UBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_UBANK_WGT_N}] ${MODELPARAM_VALUE.DPU2_UBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DPU2_UBANK_BIAS { MODELPARAM_VALUE.DPU2_UBANK_BIAS PARAM_VALUE.DPU2_UBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_UBANK_BIAS}] ${MODELPARAM_VALUE.DPU2_UBANK_BIAS}
}

proc update_MODELPARAM_VALUE.DPU2_DBANK_IMG_N { MODELPARAM_VALUE.DPU2_DBANK_IMG_N PARAM_VALUE.DPU2_DBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_DBANK_IMG_N}] ${MODELPARAM_VALUE.DPU2_DBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DPU2_DBANK_WGT_N { MODELPARAM_VALUE.DPU2_DBANK_WGT_N PARAM_VALUE.DPU2_DBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_DBANK_WGT_N}] ${MODELPARAM_VALUE.DPU2_DBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DPU2_DBANK_BIAS { MODELPARAM_VALUE.DPU2_DBANK_BIAS PARAM_VALUE.DPU2_DBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU2_DBANK_BIAS}] ${MODELPARAM_VALUE.DPU2_DBANK_BIAS}
}

proc update_MODELPARAM_VALUE.DPU3_GP_ID_BW { MODELPARAM_VALUE.DPU3_GP_ID_BW PARAM_VALUE.DPU3_GP_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_GP_ID_BW}] ${MODELPARAM_VALUE.DPU3_GP_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU3_HP0_ID_BW { MODELPARAM_VALUE.DPU3_HP0_ID_BW PARAM_VALUE.DPU3_HP0_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_HP0_ID_BW}] ${MODELPARAM_VALUE.DPU3_HP0_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU3_HP1_ID_BW { MODELPARAM_VALUE.DPU3_HP1_ID_BW PARAM_VALUE.DPU3_HP1_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_HP1_ID_BW}] ${MODELPARAM_VALUE.DPU3_HP1_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU3_HP2_ID_BW { MODELPARAM_VALUE.DPU3_HP2_ID_BW PARAM_VALUE.DPU3_HP2_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_HP2_ID_BW}] ${MODELPARAM_VALUE.DPU3_HP2_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU3_HP3_ID_BW { MODELPARAM_VALUE.DPU3_HP3_ID_BW PARAM_VALUE.DPU3_HP3_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_HP3_ID_BW}] ${MODELPARAM_VALUE.DPU3_HP3_ID_BW}
}

proc update_MODELPARAM_VALUE.DPU3_UBANK_IMG_N { MODELPARAM_VALUE.DPU3_UBANK_IMG_N PARAM_VALUE.DPU3_UBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_UBANK_IMG_N}] ${MODELPARAM_VALUE.DPU3_UBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DPU3_UBANK_WGT_N { MODELPARAM_VALUE.DPU3_UBANK_WGT_N PARAM_VALUE.DPU3_UBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_UBANK_WGT_N}] ${MODELPARAM_VALUE.DPU3_UBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DPU3_UBANK_BIAS { MODELPARAM_VALUE.DPU3_UBANK_BIAS PARAM_VALUE.DPU3_UBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_UBANK_BIAS}] ${MODELPARAM_VALUE.DPU3_UBANK_BIAS}
}

proc update_MODELPARAM_VALUE.DPU3_DBANK_IMG_N { MODELPARAM_VALUE.DPU3_DBANK_IMG_N PARAM_VALUE.DPU3_DBANK_IMG_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_DBANK_IMG_N}] ${MODELPARAM_VALUE.DPU3_DBANK_IMG_N}
}

proc update_MODELPARAM_VALUE.DPU3_DBANK_WGT_N { MODELPARAM_VALUE.DPU3_DBANK_WGT_N PARAM_VALUE.DPU3_DBANK_WGT_N } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_DBANK_WGT_N}] ${MODELPARAM_VALUE.DPU3_DBANK_WGT_N}
}

proc update_MODELPARAM_VALUE.DPU3_DBANK_BIAS { MODELPARAM_VALUE.DPU3_DBANK_BIAS PARAM_VALUE.DPU3_DBANK_BIAS } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.DPU3_DBANK_BIAS}] ${MODELPARAM_VALUE.DPU3_DBANK_BIAS}
}

proc update_MODELPARAM_VALUE.SFM_ENA { MODELPARAM_VALUE.SFM_ENA PARAM_VALUE.SFM_ENA } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SFM_ENA}] ${MODELPARAM_VALUE.SFM_ENA}
}

proc update_MODELPARAM_VALUE.SFM_HP0_ID_BW { MODELPARAM_VALUE.SFM_HP0_ID_BW PARAM_VALUE.SFM_HP0_ID_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SFM_HP0_ID_BW}] ${MODELPARAM_VALUE.SFM_HP0_ID_BW}
}

proc update_MODELPARAM_VALUE.SFM_HP_DATA_BW { MODELPARAM_VALUE.SFM_HP_DATA_BW PARAM_VALUE.SFM_HP_DATA_BW } {
	# Procedure called to set VHDL generic/Verilog parameter value(s) based on TCL parameter value
	set_property value [get_property value ${PARAM_VALUE.SFM_HP_DATA_BW}] ${MODELPARAM_VALUE.SFM_HP_DATA_BW}
}

