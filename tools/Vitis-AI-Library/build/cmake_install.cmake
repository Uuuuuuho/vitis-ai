# Install script for directory: /home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/home/uho/downloaded/petalinux_sdk/sysroots/aarch64-xilinx-linux/install/Debug")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "TRUE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/share/cmake/vitis_ai_library" TYPE FILE FILES
    "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/vitis_ai_library-config.cmake"
    "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/vitis_ai_library-config-version.cmake"
    )
endif()

if(NOT CMAKE_INSTALL_LOCAL_ONLY)
  # Include the install script for each subdirectory.
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/usefultools/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/benchmark/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/model_config/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/math/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/runner_helper/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/dpu_task/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/cpu_task/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/xnnpp/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/classification/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/tfssd/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facedetect/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facefeature/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/lanedetect/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/yolov2/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/yolov3/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facelandmark/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facequality5pt/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/ssd/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/segmentation/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/covid19segmentation/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/3Dsegmentation/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/refinedet/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/openpose/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/hourglass/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/posedetect/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/reid/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/reidtracker/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/multitask/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/platedetect/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/platenum/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/platerecog/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/carplaterecog/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/medicalsegmentation/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/medicaldetection/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facerecog/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facedetectrecog/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/pointpillars/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/retinaface/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/cifar10classification/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/mnistclassification/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/medicalsegcell/cmake_install.cmake")
  include("/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/general1/cmake_install.cmake")

endif()

if(CMAKE_INSTALL_COMPONENT)
  set(CMAKE_INSTALL_MANIFEST "install_manifest_${CMAKE_INSTALL_COMPONENT}.txt")
else()
  set(CMAKE_INSTALL_MANIFEST "install_manifest.txt")
endif()

string(REPLACE ";" "\n" CMAKE_INSTALL_MANIFEST_CONTENT
       "${CMAKE_INSTALL_MANIFEST_FILES}")
file(WRITE "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/${CMAKE_INSTALL_MANIFEST}"
     "${CMAKE_INSTALL_MANIFEST_CONTENT}")
