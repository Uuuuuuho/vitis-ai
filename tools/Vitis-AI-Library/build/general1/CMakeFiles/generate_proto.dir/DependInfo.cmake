# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/general1/test/generate_proto.cpp" "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/general1/CMakeFiles/generate_proto.dir/test/generate_proto.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "GFLAGS_IS_A_DLL=0"
  "GOOGLE_GLOG_DLL_DECL="
  "GOOGLE_GLOG_DLL_DECL_FOR_UNITTESTS="
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../benchmark/include"
  "../general1/include"
  "general1"
  "../model_config/include"
  "model_config"
  "../refinedet/include"
  "refinedet"
  "../xnnpp/include"
  "xnnpp"
  "../math/include"
  "math"
  "../dpu_task/include"
  "../facefeature/include"
  "facefeature"
  "../segmentation/include"
  "segmentation"
  "../reid/include"
  "reid"
  "../multitask/include"
  "multitask"
  "../medicalsegmentation/include"
  "medicalsegmentation"
  "../posedetect/include"
  "posedetect"
  "../platenum/include"
  "platenum"
  "../platedetect/include"
  "platedetect"
  "../facelandmark/include"
  "facelandmark"
  "../lanedetect/include"
  "lanedetect"
  "../ssd/include"
  "ssd"
  "../tfssd/include"
  "tfssd"
  "../yolov2/include"
  "yolov2"
  "../yolov3/include"
  "yolov3"
  "../classification/include"
  "classification"
  "../facedetect/include"
  "facedetect"
  "../facedetectrecog/include"
  "facedetectrecog"
  "/home/uho/downloaded/petalinux_sdk/sysroots/aarch64-xilinx-linux/install/Debug/include"
  "/home/uho/downloaded/petalinux_sdk/sysroots/aarch64-xilinx-linux/usr/include/opencv"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/general1/CMakeFiles/general1.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/refinedet/CMakeFiles/refinedet.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/segmentation/CMakeFiles/segmentation.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/reid/CMakeFiles/reid.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/multitask/CMakeFiles/multitask.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/medicalsegmentation/CMakeFiles/medicalsegmentation.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/posedetect/CMakeFiles/posedetect.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/platenum/CMakeFiles/platenum.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/platedetect/CMakeFiles/platedetect.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/lanedetect/CMakeFiles/lanedetect.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/ssd/CMakeFiles/ssd.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/tfssd/CMakeFiles/tfssd.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/yolov2/CMakeFiles/yolov2.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/yolov3/CMakeFiles/yolov3.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/classification/CMakeFiles/classification.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facedetectrecog/CMakeFiles/facedetectrecog.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facefeature/CMakeFiles/facefeature.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facelandmark/CMakeFiles/facelandmark.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/facedetect/CMakeFiles/facedetect.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/xnnpp/CMakeFiles/xnnpp.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/dpu_task/CMakeFiles/dpu_task.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/model_config/CMakeFiles/model_config.dir/DependInfo.cmake"
  "/home/uho/workspace/Vitis-AI/tools/Vitis-AI-Library/build/math/CMakeFiles/math.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
