/* 
 this file is for the configuration of thread number in pre/post processing task.
 */

#define TRUE 1
#define FALSE 0
#define SINGLE_THREAD 2
#define FOUR_THREAD 4

// implementation
#define ORIG_IMP TRUE

// pre-processing
#define ORIG_PRE_THNUM FALSE

// post-processing
#define ORIG_POST_THNUM SINGLE_THREAD